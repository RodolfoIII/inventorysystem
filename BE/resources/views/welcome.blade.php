<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>
         <meta name = "csrf-token" content="{{csrf_token()}}">
         <meta name = "url" content="{{ url('') }}">

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

       
        <style>
          
body{
margin: 0px; 
padding: 0px; 

}
        </style>
    </head>
    <body>
        <div>
           <div class ="content">
               <div id="app">
               
               @yield('content')
               </div>
           </div>
        </div>

        <script src="{{asset('js/app.js')}}"></script>
    </body>
</html>
